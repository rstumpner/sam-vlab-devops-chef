#
# Cookbook:: kanboard
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.
# Create a Message of the Day 

file '/etc/motd' do 
    content 'Managed by Chef'
end

# Update APT Cache

apt_update 'Update the apt cache daily' do
  frequency 86_400
  action :periodic
end

# Install Apache Package install-apache.rb

package 'Install Apache' do
  case node[:platform]
  when 'redhat', 'centos'
    package_name 'httpd'
  when 'ubuntu', 'debian'
    package_name 'apache2'
  end
end

# Check Apache Service service-apache.rb
service "apache2" do
  action :start
end

# Create Website
file '/var/www/html/index.html' do
  content '<html>
  <body>
    <h1>hello SAM</h1>
  </body>
</html>'
end
