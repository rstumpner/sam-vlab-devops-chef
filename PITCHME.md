<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->


# Systemadministraton 2 Übung
* SAM 2 Übung
* Devops


---
# Chef
* Devops Chef
* Version 20180506

---
# Chef Übung Übersicht
* Voraussetzungen vLAB
* Chef Einführung
* Chef First Steps
* Aufgaben Chef Level 1

---
# Chef Voraussetzungen vLAB
* VM ( Virtualbox / Vmware Workstation / Proxmox VM)
* 1 x CPU Core
* 4 GB RAM
* Linux Ubuntu 16.04 LTS
---
## Installation Chef (Manual)
* Ubuntu 16.04 LTS Basis
* Install Chef:
```bash
sudo apt-get update
sudo apt-get install -y git
sudo apt-get install -y curl
sudo apt-get install -y links
sudo apt-get install -y tree
sudo wget https://packages.chef.io/files/stable/chefdk/1.3.43/ubuntu/16.04/chefdk_
1.3.43-1_amd64.deb
sudo dpkg -i chefdk_1.3.43-1_amd64.deb
```
---
## Setup the Virtual Lab Environment
### (Vagrant)
* git clone https://www.gitlab.com/rstumpner/sam-vlab-devops-chef
* cd sam-vlab-devops-chef/vlab/vagrant-virtualbox
* vagrant up
* vagrant status
* vagrant ssh chef

---
## Einführung Chef
* Ist ein Framework um Software Auszurollen
* Desired State Configuration (Make it so)
* Declerative DSL
* Based on Ruby (Erweiterung)
* Client / Server Architektur (ChefDK / Server / Client)
* Chef Omnibus
---
## Begriffe
* Recipes
* Cookbooks
* Resources
* Databags
---
## First Steps in Chef
Check Chef Agent Version:
```bash
chef-solo -v
```
* Create Resource File (first-chef.rb)
```ruby
file '/etc/motd' do
content 'Managed by Chef'
end
```
---
## Erstes Rezept Ausführen

sudo chef-apply first-recipe.rb

* Alternativ
chef-client --local-mode first-recipe.rb

---
## Chef Resource Packages
```ruby
# APT Update (first-chef.rb)
apt_update 'Update the apt cache daily' do
frequency 86_400
action :periodic
end
```
---
## Chef Resource

```ruby
# Install Apache Package install-apache.rb
package 'Install Apache' do
	case node[:platform]
	when 'redhat', 'centos'
		package_name 'httpd'
	when 'ubuntu', 'debian'
		package_name 'apache2'
	end
end
```

---
## Chef Resource Service:
```ruby
# Check Apache Service service-apache.rb
service "apache2" do
	action :start
end
```

---
## Syntax Check mit cookstyle

cookstyle first-chef.rb

---
# Generate Chef Cookbook Repository

chef generate repo chef-repo

---
# Create your First Chef Cookbook Kanboard
cd chef-repo
chef generate cookbook cookbooks/kanboard

---
## Copy first-chef.rb to your Cookbook Recipe

cat first-chef.rb >> chef-repo/cookbooks/kanboard/recipes/default.rb

sudo chef-client --local-mode --runlist 'recipe[kanboard]'

---
# Edit Cookbook Metadata

```Ruby
chef-repo/cookbooks/kanboard/metadata.rb
name 'kanboard'
maintainer 'FH-OOE IT Gmbh'
maintainer_email 'roland@stumpner.at'
license 'All rights reserved'
description 'Installs/Configures kanboard'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '0.1.0'
depends "apache2"
depends "php"
```

---
## Download the Basics from the OSS Community

Chef Community (https://supermarket.chef.io/cookbooks)

cd chef-repo/cookbook/
knife cookbook site download apache2
tar -zxf apache2-3.1.0.tar.gz
rm apache2-3.1.0.tar.gz

---
## Aufgaben Chef
#### Level 1

Installiere einen Web Application Stack mit einer Aplikation

##### Beispiel:
* LAMP
	* Kanboard (https://kanboard.org/)

---
# Chef Cheat Sheet
* Chef Version (chef-solo -v )
* Run Recipe (chef-apply first-chef.rb | chef-client --local-mode first-chef.rb)
* Check Recipe Syntax ( cookstyle first-chef.rb)
* Run Cookbook (chef-client --local-mode --runlist 'recipe[kanboard]' )
* Create Chef Repo (chef generate repo chef-repo)
* Create a Cookbook (knife cookbook create kanboard)
* Download a Cookbook from Supermarket (knife cookbook site download
apache2)
---
# Links
* https://learn.chef.io/
* https://docs.chef.io
