<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->


# Systemadministraton 2 Übung
* SAM 2 Übung
* Devops


---
# Docker
* Devops Docker
* Version 20161027

---
# Docker Übung Übersicht
* Docker Voraussetzungen vLAB
* Docker Einführung
* Docker First Steps
* Docker Aufgaben
---
# Docker Brainstorming
* Docker Lösung
* Using Docker
* Docker Deployment
* Create your own Repo
* Versionierung
---
# LVM Voraussetzungen vLAB
* VM ( Virtualbox / Vmware Workstation / Proxmox VM)
* 1 x CPU Core
* 2 GB RAM
* 2 x VM Disks mininum 2 GB
* Linux Ubuntu 16.04 LTS
---
# Docker Einführung
* Ist ein Framework zum Deployment von Server / Web Anwendungen auf Containerbasis
* Basistechnologie Container ( LXC / Jails usw.)
* Basistechnologie COW/Layerd Dateisysteme (Unionfs/aufs)

---
# Docker Einführung Installation (Manual)
* Ubuntu 16.04 LTS Basis
* sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
* echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list
* apt-get update
* Install Optimierte Kernel Erweiterungen sudo apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual
* sudo apt-get install docker-engine
* Enable Docker Startup sudo systemctl enable docker
---
# Docker with vLAB (Vagrant)
* git pull https://www.github.com/stumpi/vLab/sam-vlab-vagrant-docker.git
*

---
# Alternativ
* sudo su
* wget -qO- https://get.docker.com/ | sh

---
# First Steps in Puppet
* Check Puppet Agent Version:
	* /opt/puppetlabs/bin/puppet agent --version
* Puppet Resources
	* Packages /opt/puppetlabs/bin/puppet resource package cron
	```md
      package { 'cron':
         ensure => '3.0pl1-128ubuntu2',
       }
   ```
   * Files /opt/puppetlabs/bin/puppet resource file /etc/motd
   ```md
      file { '/etc/motd':
        ensure => 'absent',
      }
    ```
   * Services /opt/puppetlabs/bin/puppet resource service cron
   ```md
      service { 'cron':
        ensure => 'running',
        enable => 'true',
      }
   ```

---
# Docker First Steps
* docker --version (1.12.3)
* sudo docker run hello-world (Verify Install)
* First Real Container: sudo docker run -it ubuntu bash
* List Installed Containers: docker ps
---
# Docker Container Playground (Whalesay)
* Is a Container Persistant ???
* touch test
* touch test123
* Commit an Image
* docker run -it ubuntu bash
* sudo apt-get install apache2
* Notiere Container ID: 9a0874f113e6
* sudo docker commit -m "Added Apache2" -a "Roland Stumpner" 9a0874f113e6 roland/ubuntu-apache2-manual
* sudo docker run -p 80:80 -t -i roland/ubuntu-apache2-manual /bin/bash
*
---
# Docker Theory
* Skizze Layerd Filesystem
* Skizze Ökosystem / Repository
* Stateless
---
# Docker Aufgaben
* Setup a LAMP Environment with Docker
* Create a Docker Container for Kanboard
---
# Docker Lösung
* mkdir Firstcontainer
* touch Dockerfile
* Edit Dockerfile:
 FROM ubuntu:latest
 RUN apt-get -y update && apt-get install -y apache2
 CMD service apache2 start
* sudo docker build -t ubuntu-apache2 .
---
# Docker Lösung
*
---
# Docker Cheat Sheet
* List Images (docker images)
* Run Latest Ubuntu Image (sudo docker run -t -i ubuntu:latest /bin/bash)
* Download a new Container ( docker pull kanoard\kanboard
* Show Running Containers (sudo docker ps)
* Stop a Container (sudo docker stop kanoard)
* Remove Containers not running (docker rm `docker ps -aq -f status=exited`)
* Mount a Directory from Docker Host to Container (sudo docker run -d -p 80:80 -v /home/roland/Dokumente/privat/hausbau/kanboard/data:/var/www/app/data -t kanboard/kanboard:latest)
*
---
# Notizen
 docker run hello-world
    2  service docker start
    3  sudo service docker start
    4  docker run hello-world
    6  docker run -it ubuntu bash
    7  sudo docker run -it ubuntu bash
    8  docker status
    9  docker --help
   10  docker info
   11  sudo docker info
   12  sudo docker info help
   13  sudo docker info --help
   14  sudo docker info
   15  docker help
   16  docker ps
   17  sudo docker ps
   18  sudo docker info
   19  sudo docker help
   20  sudo docker stats
   21  sudo docker images
   22  ls
   23  sudo docker info
   24  sudo docker help
   25  sudo docker ps
   26  sudo docker stop
   27  sudo docker info
   28  sudo docker stop hello-world
   29  sudo docker logs
   30  sudo docker logs hello-wordl
   31  sudo docker logs hello-world
   32  ps -awx
   33  sudo docker images
   34  histoy
   35  history
   36  sudo docker help
   37  sudo docker run -it ubuntu bash
   38  sudo docker run ubuntu
   39  sudo docker start ubuntu
   40  sudo docker start
   41  sudo docker run ubuntu
   42  sudo docker info
   43  sudo docker ps
   44  sudo docker start ubuntu
   45  exit
